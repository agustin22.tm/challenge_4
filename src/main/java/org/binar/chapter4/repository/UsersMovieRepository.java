package org.binar.chapter4.repository;

import org.binar.chapter4.model.UsersMovie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface UsersMovieRepository extends JpaRepository<UsersMovie, String> {

    //Service addNewFilm
    @Modifying
    @Query(value = "insert into users_movie (email, password, username) values (:email,:password, :username)", nativeQuery = true)
     void insertUserToDb(@Param("email") String email,
                         @Param("password") String password,
                         @Param("username")String username);

    //Service deleteUser
    @Modifying
    @Query(value = "delete from users_movie u where u.username = :username", nativeQuery = true)
    void deleteUserFromDb(@Param("username") String username);

    //Service updateUser
    @Modifying
    @Query(value = "update users_movie set email = :email, password = :password where username = :username", nativeQuery = true)
    void updateUserToDb(@Param("email") String email,
                        @Param("password") String password,
                        @Param("username")String username);
}
