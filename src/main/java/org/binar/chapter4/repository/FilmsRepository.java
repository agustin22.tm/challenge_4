package org.binar.chapter4.repository;

import org.binar.chapter4.model.Films;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.*;

@Transactional
@Repository //Identifier bean
public interface FilmsRepository extends JpaRepository<Films, Integer> {

    @Modifying
    @Query(value = "insert into films (film_name, showing) values (:name, :showing)", nativeQuery = true)
     void insertFilmToDb(@Param("name") String filmName,
                         @Param("showing") Boolean showing);


    //Service showingFilm
    @Query(value = "SELECT f FROM Films f WHERE showing = :showing", nativeQuery = true)
     List<Films> findFilmByShowing(@Param("showing") Boolean showing);


    //Service deleteFilm
    @Modifying
    @Query(value = "delete from films f where f.film_name = :name", nativeQuery = true)
     void deleteFilmFromDb(@Param("name") String filmName);

    //Service updateFilm
    @Modifying
    @Query(value = "update films set film_name = :name, showing = :showing where film_code = :code", nativeQuery = true)
     void updateFilmToDb(@Param("name") String filmName,
                         @Param("showing") Boolean showing,
                         @Param("code") Integer filmCode);

}
